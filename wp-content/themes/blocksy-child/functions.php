<?php

if (! defined('WP_DEBUG')) {
	die( 'Direct access forbidden.' );
}

add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_script('my-script', get_template_directory_uri() . '/resi.js', array('jquery'));
	wp_localize_script( 'my-script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
});

function track_form() {
	return '<div id="tracking-container">
<form type="post" class="tracking_form">
	<input id="no_resi" class="trc_no" placeholder="Nomor Resi Pengiriman" name="no_resi" required="" type="text">
	<select name="ekspedisi" id="ekspedisi" class="trc_eks">
		<option value="JNE">Jalur Nugraha Ekakurir (JNE)</option>
		<option value="TIKI">Citra Van Titipan Kilat (TIKI)</option>
		<option value="JNT">J&amp;T Express (J&amp;T)</option>
		<option value="POS">POS Indonesia (POS)</option>
		<option value="LION">Lion Parcel (LION)</option>
		<option value="NINJA">Ninja Xpress (NINJA)</option>
		<option value="SICEPAT">SiCepat Express (SICEPAT)</option>
		<option value="IDE">ID Express (IDE)</option>
		<option value="SAP">SAP Express (SAP)</option>
		<option value="NCS">Nusantara Card Semesta (NCS)</option>
		<option value="ANTERAJA">AnterAja (ANTERAJA)</option>
		<option value="REX">Royal Express Indonesia (REX)</option>
		<option value="SENTRAL">Sentral Cargo (SENTRAL)</option>
		<option value="RPX">RPX Holding (RPX)</option>
		<option value="PANDU">Pandu Logistics (PANDU)</option>
		<option value="WAHANA">Wahana Prestasi Logistik (WAHANA)</option>
		<option value="PAHALA">Pahala Kencana Express (PAHALA)</option>
		<option value="JET">JET Express (JET)</option>
		<option value="SLIS">Solusi Ekspres (SLIS)</option>
		<option value="EXPEDITO">Expedito* (EXPEDITO)</option>
		<option value="DSE">21 Express (DSE)</option>
		<option value="FIRST">First Logistics (FIRST)</option>
		<option value="STAR">Star Cargo (STAR)</option>
		<option value="IDL">IDL Cargo (IDL)</option>
	</select>
	<input type="submit" name="submit" id="submit" value="Check Resi" class="trc_button">
</form>
</div>
<div id="ck-content" class="ck-content"></div>';
}

function csrf_token() {
	// if (! isset($_SESSION['csrf_token'])) {
    	$_SESSION['csrf_token'] = bin2hex(random_bytes(35));
    	return $_SESSION['csrf_token'];
	// }
}


add_shortcode( 'wp_track_form', 'track_form' );

//add_action('wp_ajax_tracking_action', 'tracking');
if ( is_user_logged_in() ) {
	add_action( 'wp_ajax_tracking_action', 'tracking');
} else {
	add_action( 'wp_ajax_nopriv_tracking_action', 'tracking');
}

function tracking() {
	  	$no_resi = $_POST['no_resi'];
	  	$expedisi = "jne";
//print_r($no_resi);exit();
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "waybill=" . $no_resi . "&courier=" . strtolower($expedisi),
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: 7d128f1381c4001f36abaa9846b1c829"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		show_result($response);
	  	wp_die();
}

function show_result($response) {
	$array = json_decode($response, true);
	$result= isset($array['rajaongkir']) ? $array['rajaongkir'] : [];


	if($result['status']['code'] == 200) {
		echo "<h2>Informasi Pengiriman</h2>";
		echo getSummaries($result['result']['summary']);
		echo "<h2>Riwayat Pengiriman</h2>";
		echo getDeliveryTime($result['result']['manifest']);
	} else {
		echo "No Resi tidak ditemukan!";
	}
}

function getSummaries($data) {
	return "<table class='table'>
    <thead>
    <tbody>
      <tr>
        <td>No Resi</td>
        <td>" . $data['waybill_number'] . "</td>
	  </tr>
	  <tr>
        <td>Status</td>
        <td><strong>" . $data['status'] . "</strong></td>
	  </tr>
	  <tr>
        <td>Service</td>
        <td>" . $data['service_code'] . "</td>
	  </tr>
	  <tr>
        <td>Di kirim Tanggal</td>
        <td>" . $data['waybill_date'] . "</td>
	  </tr>
	  <tr>
        <td>Dikirim ke</td>
        <td>" . $data['receiver_name'] . "</td>
	  </tr>
  	</tbody>
  	</table>";
}

function getDeliveryTime($data) {
  	$html = "<table class='table'>";
  	$html .= "<thead>
    	<tr>
    		<th>Tanggal</th>
    		<th>Keterangan</th>
		</tr>
    </thead>";
	foreach($data as $result){
        $html .= '<tr>';
        $html .= '<td>' . $result['manifest_date'] . ' ' . $result['manifest_time'] . '</td>';
        $html .= '<td>' . $result['manifest_description'] .'</td>';
        $html .= '</tr>';
	}
	$html .= "</table>";
	return $html;
}
