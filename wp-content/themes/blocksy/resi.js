jQuery(document).ready(function($) {

  $('#tracking-container form').on('submit', function() {
    var no_resi =$("#no_resi").val();
    var ekspedisi =$("#ekspedisi").val();
    var $content = $('#ck-content')

    $.ajax({
        type : 'post',
        url : myAjax.ajaxurl,
        data : {
            action : 'tracking_action',
            no_resi : no_resi,
            expedisi: ekspedisi
        },
        beforeSend: function() {
            // $input.prop('disabled', true);
            $content.html('Harap Tunggu.. kami sedang menghubungi pihak ekspedisi.');
        },
        success : function( response ) {
            // $input.prop('disabled', false);
            $content.html('');
            $content.append( response );
            $("#no_resi").val('');
        }
    });

    return false;
  })
});
