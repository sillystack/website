<?php
$origin = $_GET['origin'];
$destination = $_GET['destination'];
$weight = $_GET['weight'];
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "origin=".$origin."&originType=subdistrict&destination=".$destination."&destinationType=subdistrict&weight=".$weight."&courier=ninja:sicepat:jne:pos:tiki:jnt:lion:ide:anteraja:wahana:jet",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/x-www-form-urlencoded",
    "key: 7d128f1381c4001f36abaa9846b1c829"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

$result = json_decode($response);
$data = $result->rajaongkir->results;
$couriers = array();
foreach ($data as $courier) {
    if(!empty($courier->costs)){
        foreach ($courier->costs as $cost) {
            $cost->code = $courier->code;
            $cost->name = $courier->name;
            $couriers[] = $cost;
        }
    }
}
$response = array("data" => $couriers);
echo json_encode($response);
