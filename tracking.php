<?php
$no_resi = $_POST['no_resi'];
$expedisi = $_POST['expedisi'];
$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => "waybill=" . $no_resi . "&courier=" . strtolower($expedisi),
	CURLOPT_HTTPHEADER => array(
		"content-type: application/x-www-form-urlencoded",
		"key: 7d128f1381c4001f36abaa9846b1c829"
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);
echo $response;
?>
