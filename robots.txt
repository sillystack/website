User-Agent: *
Allow: /wp-content/uploads/
Disallow: /wp-content/plugins/
Disallow: /wp-admin/
Disallow: /readme.html
Disallow: /refer/
 
Sitemap: https://www.sillystack.com/post-sitemap.xml
Sitemap: https://www.sillystack.com/page-sitemap.xml